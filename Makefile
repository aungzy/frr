IMAGE_NAME=localhost/frr
.PHONY: build
build:
	docker build -t $(IMAGE_NAME) .

.PHONY: push
push:
	docker push $(IMAGE_NAME)

.PHONY: build_push
build_push: build push