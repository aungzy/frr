FROM docker.io/frrouting/frr:latest

RUN cp /etc/frr/vtysh.conf.sample /etc/frr/vtysh.conf && \
    sed -i -E 's/(ospf|isis)d=no/\1d=yes/' /etc/frr/daemons && \
    mkdir -p /var/log/frr && \
    chown -R frr /var/log/frr
